using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodController : MonoBehaviour
{
    private GameObject sword;

    public bool bloodPlayCmd = false;
    public Vector3 hitPoint;

    private ParticleSystem ps = null;

    private bool firstFrame = true;

    private void FindSword()
    {
        GameObject[] swords = GameObject.FindGameObjectsWithTag("Sword");
        Debug.Log($"Blood effect found Swords: {swords.Length}");
        foreach (GameObject s in swords)
        {
            ParticleSystem p = s.GetComponentInChildren<ParticleSystem>();
            if (p == null)
            {
                this.sword = s;
                transform.SetParent(this.sword.transform);
                firstFrame = false;

                Vector3 org = sword.transform.position;

                transform.SetPositionAndRotation(org, sword.transform.rotation);
                transform.RotateAround(org, sword.transform.right, 90f);

                break;
            }
        }
    }
    private void Start()
    {
        ps = GetComponentInChildren<ParticleSystem>();

        FindSword();

        //transform.SetParent(attachedPlayer.transform);
    }
    void Update()
    {
        if (sword == null && firstFrame)
        {
            FindSword();
            return;
        }
        if (ps != null && sword != null)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                bloodPlayCmd = true;
            }

            if (bloodPlayCmd)
            {
                transform.SetPositionAndRotation(hitPoint, sword.transform.rotation);
                transform.RotateAround(hitPoint, sword.transform.right, 90f);
                if (ps.isPlaying)
                {
                    ps.Stop();
                }
                ps.Play();
                //Debug.Log("Play Blood simulation");
                bloodPlayCmd = false;
            }
        }

    }
}
