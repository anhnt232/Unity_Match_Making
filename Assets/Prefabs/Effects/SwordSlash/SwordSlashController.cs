using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class SwordSlashController : MonoBehaviour
{
    private GameObject sword;

    public bool playCmd = false;

    private VisualEffect visualEffect = null;

    private bool firstFrame = true;

    private void FindSword()
    {
        GameObject[] swords = GameObject.FindGameObjectsWithTag("Sword");
        Debug.Log($"Slash effect found Swords: {swords.Length}");
        foreach (GameObject s in swords)
        {
            VisualEffect v = s.GetComponentInChildren<VisualEffect>();
            if (v == null)
            {
                this.sword = s;
                transform.SetParent(this.sword.transform);
                firstFrame = false;

                Vector3 org = sword.transform.position;

                transform.SetPositionAndRotation(org, sword.transform.rotation);
                transform.RotateAround(org, sword.transform.right, 90f);

                break;
            }
        }
    }
    private void Start()
    {
        visualEffect = GetComponentInChildren<VisualEffect>();

        FindSword();

        //transform.SetParent(attachedPlayer.transform);
    }
    void Update()
    {
        if(sword == null && firstFrame)
        {
            FindSword();
            return;
        }
        if (visualEffect != null && sword != null)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                playCmd = true;
            }

            if (playCmd)
            {
                visualEffect.Play();
                playCmd = false;
            }
        }

    }
}
