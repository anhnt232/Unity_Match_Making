using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.VFX;


public struct CreateMMOCharacterMessage : NetworkMessage
{
    public string username;
    public string teamColor;
    public string heroName;
};

public class MMONetworkManager : NetworkManager
{

    public GameObject Jammo, Knight, KnightKit, Ninja, Asuna, Dreyar, Kachujin, Rfa, Ely;

    private GameObject selectedHero;


    public override void OnStartServer()
    {
        base.OnStartServer();

        NetworkServer.RegisterHandler<CreateMMOCharacterMessage> (OnCreateCharacter);
    }

    public override void OnClientConnect()
    {
        base.OnClientConnect();

        // you can send the message here, or wherever else you want
        CreateMMOCharacterMessage characterMessage = new()
        {
            username = PlayerPrefs.GetString("username", "kaka"),
            teamColor = PlayerPrefs.GetString("teamColor", ""),
            heroName = PlayerPrefs.GetString("heroName", "Jammo")
            //heroName = "Ninja"
        };
        //Debug.Log("OnClientConnect send Create Char");
        NetworkClient.Send(characterMessage);

    }

    void OnCreateCharacter(NetworkConnectionToClient conn, CreateMMOCharacterMessage message)
    {
        GameObject playerGameobject;
        if (playerPrefab != null)
        {
            playerGameobject = Instantiate(playerPrefab);
        }
        else
        {
            playerGameobject = Instantiate(ConnectWithSelectedCharacter(message.heroName));
        }
        
        PlayerControllerComponent playerController = playerGameobject.GetComponent<PlayerControllerComponent>();
        playerController.name = $"P_{message.username}_C{message.teamColor}_{conn}";
        Debug.Log(playerController.name + " is Created");
        playerController.SetName(message.username);
        playerController.SetColor(message.teamColor);

        //GameObject swordPrefab = Instantiate(spawnPrefabs[0]);
        GameObject swordBloodFx = Instantiate(spawnPrefabs[1]);
        GameObject swordSlashVfx = Instantiate(spawnPrefabs[2]);


        NetworkServer.AddPlayerForConnection(conn, playerGameobject);
        //NetworkServer.Spawn(swordPrefab, playerGameobject);
        NetworkServer.Spawn(swordBloodFx, playerGameobject);
        NetworkServer.Spawn(swordSlashVfx, playerGameobject);

    }

    public GameObject ConnectWithSelectedCharacter(string heroName)
    {
        selectedHero = heroName switch
        {
            "Jammo" => Jammo,
            "Knight" => Knight,
            "KnightKit" => KnightKit,
            "Ninja" => Ninja,
            "Asuna" => Asuna,
            "Dreyar" => Dreyar,
            "Kachujin" => Kachujin,
            "Rfa" => Rfa,
            "Ely" => Ely,
            _ => Jammo,
        };
        return selectedHero;
    }

}
