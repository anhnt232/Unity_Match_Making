using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class MMONetworkManagerGUI : MonoBehaviour
{
    NetworkManager manager;

    public int offsetX;
    public int offsetY;

    private Menu menu;
    private Button stopButton;
    private CharacterImageControl characterImageControl;

    private int selectedCharIdx = 0;
    public bool showDebugUI = false;

    void Awake()
    {
        manager = GetComponent<NetworkManager>();
        if (menu == null)
        {
            menu = GameObject.FindWithTag("Menu").GetComponent<Menu>();
            stopButton = GameObject.FindWithTag("StopButton").GetComponent<Button>();
            stopButton.onClick.AddListener(() => {
                manager.StopClient();
                menu.gameObject.SetActive(true);
            });

            if (menu != null)
            {
                menu.onChangeUsername.AddListener((string newUsername) => { SetUserName(newUsername); });

                menu.onChangeColor.AddListener((string newColor) => { SetColor(newColor); });

                menu.onChangeNext.AddListener((string currentIndex) => { NextCharacterSelect(); });

                menu.onChangePres.AddListener((string newUsername) => { PresCharacterSelect(); });

                menu.onPlayGame.AddListener((string newUsername) => {
                    //Debug.Log("Play Game Pressed");
                    manager.StartClient();
                    menu.gameObject.SetActive(false);
                });

                menu.Init();
                characterImageControl = GameObject.FindWithTag("CharacterSelectionImage").GetComponent<Image>().GetComponent<CharacterImageControl>();
                selectedCharIdx = PlayerPrefs.GetInt("currentHeroIdx", 0);
            }
        }
    }

    private void NextCharacterSelect()
    {
        selectedCharIdx++;
        selectedCharIdx = characterImageControl.DisplayCharacterImage(selectedCharIdx);
    }
    private void PresCharacterSelect()
    {
        selectedCharIdx--;
        selectedCharIdx = characterImageControl.DisplayCharacterImage(selectedCharIdx);
    }
    public void SetUserName(string username)
    {
        PlayerPrefs.SetString("username", username);
    }

    public void SetColor(string color)
    {
        PlayerPrefs.SetString("teamColor", color);
    }

    public void SetHeroName(string heroName)
    {
        PlayerPrefs.SetString("heroName", heroName);
    }

    void OnGUI()
    {
        if (showDebugUI)
        {
            GUILayout.BeginArea(new Rect(10 + offsetX, 40 + offsetY, 215, 100));
            if (!NetworkClient.isConnected && !NetworkServer.active)
            {
                StartButtons();
            }
            else
            {
                StatusLabels();
            }

            // client ready
            if (NetworkClient.isConnected && !NetworkClient.ready)
            {
                if (GUILayout.Button("Client Ready"))
                {
                    NetworkClient.Ready();
                    if (NetworkClient.localPlayer == null)
                    {
                        NetworkClient.AddPlayer();
                    }
                }
            }

            StopButtons();

            GUILayout.EndArea();
        }
        //else
        //{
        //    if (!NetworkClient.isConnected)
        //    {
        //        menu.gameObject.SetActive(true);
        //        menu.gameTitle.text = "Disconnected!, please Replay";
        //    }
        //}
    }

    void StartButtons()
    {
        if (!NetworkClient.active)
        {
            // Server + Client
            if (Application.platform != RuntimePlatform.WebGLPlayer)
            {
                if (GUILayout.Button("Host (Server + Client)"))
                {
                    manager.StartHost();

                    menu.gameObject.SetActive(false);
                }
            }

            // Client + IP
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Client"))
            {
                manager.StartClient();

                menu.gameObject.SetActive(false);
            }
            // This updates networkAddress every frame from the TextField
            manager.networkAddress = GUILayout.TextField(manager.networkAddress);
            GUILayout.EndHorizontal();

            // Server Only
            if (Application.platform == RuntimePlatform.WebGLPlayer)
            {
                // cant be a server in webgl build
                GUILayout.Box("(  WebGL cannot be server  )");
            }
            else
            {
                if (GUILayout.Button("Server Only")) manager.StartServer();
            }
        }
        else
        {
            // Connecting
            GUILayout.Label($"Connecting to {manager.networkAddress}..");
            if (GUILayout.Button("Cancel Connection Attempt"))
            {
                manager.StopClient();
            }
        }
    }

    void StatusLabels()
    {
        // host mode
        // display separately because this always confused people:
        //   Server: ...
        //   Client: ...
        if (NetworkServer.active && NetworkClient.active)
        {
            GUILayout.Label($"<b>Host</b>: running via {Transport.activeTransport}");
        }
        // server only
        else if (NetworkServer.active)
        {
            GUILayout.Label($"<b>Server</b>: running via {Transport.activeTransport}");
        }
        // client only
        else if (NetworkClient.isConnected)
        {
            GUILayout.Label($"<b>Client</b>: connected to {manager.networkAddress} via {Transport.activeTransport}");
        }
    }

    void StopButtons()
    {
        // stop host if host mode
        if (NetworkServer.active && NetworkClient.isConnected)
        {
            if (GUILayout.Button("Stop Host"))
            {
                manager.StopHost();

                menu.gameObject.SetActive(true);
            }
        }
        // stop client if client-only
        else if (NetworkClient.isConnected)
        {
            if (GUILayout.Button("Stop Client"))
            {
                manager.StopClient();

                menu.gameObject.SetActive(true);
            }
        }
        // stop server if server-only
        else if (NetworkServer.active)
        {
            if (GUILayout.Button("Stop Server"))
            {
                manager.StopServer();
            }
        }
    }

}

