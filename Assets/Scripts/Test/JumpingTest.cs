using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpingTest : MonoBehaviour
{
    private CharacterController controller;
    private bool isGrounded;
    private float verticalVel;
    private Vector3 moveVector;
    private Animator animator;
    private bool isJumping, isFalling;

    private float verticalVelocity;
    private float groundedTimer;        // to allow jumping when going down ramps
    private float playerSpeed = 2.0f;
    private float jumpHeight = 3f;
    private float gravityValue = 9.81f;
    private bool groundedPlayer = false;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        controller = this.GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {


        // gather lateral input control
        Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        // scale by speed
        move *= playerSpeed;

        // only align to motion if we are providing enough input
        if (move.magnitude > 0.05f)
        {
            gameObject.transform.forward = move;
        }

        // allow jump as long as the player is on the ground
        if (Input.GetKeyDown(KeyCode.J))
        {
            // must have been grounded recently to allow jump
            if (groundedPlayer)
            {
                // Physics dynamics formula for calculating jump up velocity based on height and gravity
                verticalVelocity += Mathf.Sqrt(jumpHeight * 2 * gravityValue);

                animator.SetTrigger("JumpUp");
                isJumping = true;
            }
        }

        if (!groundedPlayer)
        {
            // apply gravity always, to let us track down ramps properly
            verticalVelocity -= gravityValue * Time.deltaTime;
        }
        else
        {
            if (verticalVelocity < 0)
                verticalVelocity = 0;
        }


        if (!groundedPlayer)
        {
            if (isJumping && verticalVelocity < 0)
            {
                animator.SetTrigger("Falling");
                isFalling = true;
                isJumping = false;
            }
        }
        if (isFalling && groundedPlayer)
        {
            isFalling = false;
            animator.SetTrigger("Landing");
        }
        

        // inject Y velocity before we use it
        move.y = verticalVelocity;

        // call .Move() once only
        controller.Move(move * Time.deltaTime);
        //Debug.Log($"isJumping: {isJumping}, isGrounded: {groundedPlayer}, verticalVel: {verticalVelocity}, DeltaTime: {Time.deltaTime}, movVector: {move}");

        groundedPlayer = controller.isGrounded;

    }


}
