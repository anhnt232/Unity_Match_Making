using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverlapBox : MonoBehaviour
{
    bool mStarted;
    public LayerMask mLayerMask;
    private BoxCollider mCollider;

    void Start()
    {
        //Use this to ensure that the Gizmos are being drawn when in Play Mode.
        mStarted = true;
        mCollider = GetComponent<BoxCollider>();
        Debug.Log(mCollider.name);

    }

    void FixedUpdate()
    {
        //MyCollisions(mCollider);
        RaycastHit hit;
        Vector3 rayOrigin = transform.position;
        Vector3 rayDirection = transform.up;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(rayOrigin, rayDirection, out hit, 1.5f, mLayerMask))
        {
            Debug.DrawRay(rayOrigin, rayDirection * hit.distance, Color.yellow);
            Debug.Log($"Did Hit: {hit.transform.name}");
            if (hit.transform.name.StartsWith("Jammo"))
            {
                Debug.Log($"Did Hit JammoPlayer: {hit.transform.name}");
            }
        }
        else
        {
            Debug.DrawRay(rayOrigin, rayDirection * 1.5f, Color.white);
            //Debug.Log("Did not Hit");
        }

    }



    void MyCollisions(Collider col)
    {
        //Use the OverlapBox to detect if there are any other colliders within this box area.
        //Use the GameObject's centre, half the size (as a radius) and rotation. This creates an invisible box around your GameObject.
        Collider[] hitColliders = Physics.OverlapBox(col.bounds.center, col.bounds.extents, col.transform.rotation, mLayerMask);

        foreach(Collider c in hitColliders)
        {
            if (c.transform == transform)
                continue;

            if (c.transform.parent.parent.parent.parent.parent.parent.parent.parent.parent.parent == transform)
                continue;

            Debug.Log($"{transform.name} Hit: {c.name}");
        }
    }

    //Draw the Box Overlap as a gizmo to show where it currently is testing. Click the Gizmos button to see this
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        //Check that it is being run in Play Mode, so it doesn't try to draw this in Editor mode
        if (mStarted && mCollider != null)
        {
            //Draw a cube where the OverlapBox is (positioned where your GameObject is as well as a size)
            Gizmos.DrawWireCube(mCollider.bounds.center, mCollider.size
                );
        }

    }

}
