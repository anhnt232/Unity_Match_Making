using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMapCameraController : MonoBehaviour
{
    public float interval = 0.5f;
    public Transform targetPlayer;

    //void Awake()
    //{
    //    GetComponent<Camera>().enabled = false;
    //    InvokeRepeating("Render", 0, interval);
    //}

    //void Render()
    //{
    //    GetComponent<Camera>().Render();
    //}

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if(targetPlayer != null)
        {
            Vector3 newPosition = targetPlayer.position;
            newPosition.y = targetPlayer.position.y + 10;
            transform.position = newPosition;
            //transform.rotation = Quaternion.Euler(90f, targetPlayer.eulerAngles.y, 0f);
        }
        
    }
}
