using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordAttackComponent : MonoBehaviour
{
    private Transform swordOwnerTransform;
    private PlayerControllerComponent swordOwner;
    public LayerMask mLayerMask;
    public bool attackState = false;

    private float attackTimer = 0f;
    private float hitTimer = 0f;

    // Start is called before the first frame update
    void Start()
    {
        swordOwner = GetComponentInParent<PlayerControllerComponent>();
        if (swordOwner != null)
            swordOwnerTransform = swordOwner.transform;
        else
            Debug.Log("Not Found Player Controller Component");

        Debug.Log($"swordOwner: {swordOwner.transform.gameObject.name}");
    }

    public void SetSwordOwner(Transform t)
    {
        swordOwnerTransform = t;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        attackTimer -= Time.deltaTime;

        if (attackState)
        {
            checkHit();
            attackTimer = 1f;
        }

        if(attackTimer < 0)
        {
            attackState = false;
        }
    }

    private void checkHit()
    {
        RaycastHit hit;
        Vector3 rayOrigin = transform.position;
        Vector3 rayDirection = transform.up;
        float distance = 2.5f;
        // Does the ray intersect any objects excluding the player layer
        hitTimer -= Time.deltaTime;
        if(hitTimer < 0)
        {
            
            if (Physics.Raycast(rayOrigin, rayDirection, out hit, distance, mLayerMask))
            {
                //Debug.DrawRay(rayOrigin, rayDirection * hit.distance, Color.yellow);
                //Debug.Log($"Did Hit: {hit.transform.name}");
                if (hit.transform.CompareTag("Player"))
                {
                    PlayerControllerComponent hittenPlayer = hit.transform.GetComponent<PlayerControllerComponent>();
                    if(swordOwner != null)
                    {
                        if (hittenPlayer.transform.GetInstanceID() != swordOwner.transform.GetInstanceID())
                        {
                            PlayerHealthComponent myPlayer = swordOwner.transform.GetComponent<PlayerHealthComponent>();
                            myPlayer.attack = true;
                            myPlayer.target = hittenPlayer.gameObject;
                            //Debug.Log($"Did Hit Player name: {hittenPlayer.GetName()}");
                            // Display blood animation here

                            attackState = false;

                            BloodController b = swordOwner.transform.GetComponentInChildren<BloodController>();
                            b.hitPoint = hit.point;
                            b.bloodPlayCmd = true;

                        }
                    }
                    
                    hitTimer = 1f;
                }
            }
            //else
            //{
            //    Debug.DrawRay(rayOrigin, rayDirection * distance, Color.white);
            //    //Debug.Log("Did not Hit");
            //}
        }
    }
}
