using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using Mirror;
using UnityEngine.UI;
using TMPro;

public class PlayerControllerComponent : NetworkBehaviour
{
    private PlayerSkinComponent skinController;
    private CinemachineFreeLook freeLookCam;
    private string username;
    private string teamColor;
    private TextMeshProUGUI playerNameText;
    //private SwordAttackComponent swordAttack;
    private SpriteRenderer playerMarker;

    private NetworkAnimator netAnimator;

    [SerializeField] private PlayerHealthComponent health = null;
    [SerializeField] private Image healthBarImage = null;

    [SyncVar(hook = nameof(DisplayPlayerName))]
    public string playerDisplayName;

    [SyncVar(hook = nameof(DisplayPlayerColor))]
    public string playerColor;

    [SyncVar]
    public string displayMessage = "Hello";

    bool isDead = false;

    private void Awake()
    {
        skinController = GetComponent<PlayerSkinComponent>();

        Transform t = transform.Find("CharacterNameCanvas");
        if (t != null)
        {
            playerNameText = t.Find("Name").GetComponent<TextMeshProUGUI>();
            healthBarImage = t.Find("BarHealth").GetComponent<Image>();
        }

        health = GetComponent<PlayerHealthComponent>();

        netAnimator = this.GetComponent<NetworkAnimator>();

        playerMarker = transform.Find("CharacterMarker").GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Start Player " + this.name);
        if (!hasAuthority)
        {
            return;
        }
    }

    private void Update()
    {
        if (!hasAuthority) { return; }

        if (Input.GetKeyDown(KeyCode.Y))
        {
            CmdSendName(this.username, this.teamColor);
        }
    }


    [Command]
    public void CmdSendName(string playerName, string color)
    {
        playerDisplayName = playerName;
        playerColor = color;

        // no rpc needed because the magic of SyncVar.
        // a syncvar listen to data that is on the server
        // thus, we change data on the server and ALL clients update the name with the use of the hook.
    }


    public void DisplayPlayerName(string oldName, string newName)
    {
        //Debug.Log($"Player {this.name} changed name from " + oldName + " to " + newName);
        SetName(newName);
    }

    public void DisplayPlayerColor(string oldColor, string newColor)
    {
        SetColor(newColor);
    }

    public override void OnStartLocalPlayer()
    {
        freeLookCam = GameObject.FindObjectOfType<CinemachineFreeLook>();
        freeLookCam.Follow = this.transform;
        freeLookCam.LookAt = this.transform;

        this.username = PlayerPrefs.GetString("username", "kaka");
        this.teamColor = PlayerPrefs.GetString("teamColor", "");

        SetName(this.username);
        SetColor(this.teamColor);

        Debug.Log(this.name.ToString() + " called: " + this.username + " is local Player");

        CmdSendName(this.username, this.teamColor);
    }


    public void SetName(string username)
    {
        //this.username = username;
        playerNameText.SetText(username);
    }

    public string GetName()
    {
        //this.username = playerNameText.text;
        return playerNameText.text;
    }

    public void SetColor(string color)
    {
        if (color == "red")
        {
            if (skinController != null)
                skinController.ChangeMaterialSettings(0);
            playerNameText.color = Color.red;
            playerMarker.color = Color.red;
            healthBarImage.color = Color.red;
        }
        if (color == "black")
        {
            if (skinController != null)
                skinController.ChangeMaterialSettings(1);
            playerNameText.color = Color.black;
            playerMarker.color = Color.black;
            healthBarImage.color = Color.black;
        }
        if (color == "blue")
        {
            if (skinController != null)
                skinController.ChangeMaterialSettings(2);
            playerNameText.color = Color.blue;
            playerMarker.color = Color.blue;
            healthBarImage.color = Color.blue;
        }
        if (color == "yellow")
        {
            if (skinController != null)
                skinController.ChangeMaterialSettings(3);
            playerNameText.color = Color.yellow;
            playerMarker.color = Color.yellow;
            healthBarImage.color = Color.yellow;
        }
    }
    public string GetColor()
    {
        return this.teamColor;
    }


    private void OnEnable()
    {
        health.onHealthChanged += HandleHealthChanged;
    }

    private void OnDisable()
    {
        health.onHealthChanged -= HandleHealthChanged;
    }

    void HandleHealthChanged(int currentHealth, int maxHealth, string attackerName)
    {
        healthBarImage.fillAmount = (float)currentHealth / maxHealth;
        Debug.Log($"{GetName()} has been Attacked by {attackerName}");
        if (currentHealth == 0)
        {
            if (hasAuthority && !isDead)
            {
                //netAnimator.SetTrigger("Dead");
                isDead = true;
                displayMessage = $"{GetName()} has been KILLED by {attackerName}";
                Debug.Log(displayMessage);
                //NetworkServer.Destroy(gameObject);
                netAnimator.SetTrigger("Dead");
                StartCoroutine(Destroy());
            }
        }

    }

    public float timeToDestroy = 4f;


    IEnumerator Destroy()
    {

        //yield on a new YieldInstruction that waits for n seconds.
        yield return new WaitForSeconds(timeToDestroy);

        NetworkServer.Destroy(this.gameObject);
    }


    void OnGUI()
    {
        GUIStyle style = new GUIStyle();
        style.fontSize = 50;
        style.normal.textColor = Color.white;

        GUILayout.BeginArea(new Rect(Screen.width/2 - 5, 5, Screen.width/2, 50));
        GUILayout.Label(displayMessage, style);
        GUILayout.EndArea();
    }
}
