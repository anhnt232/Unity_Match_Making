﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSkinComponent : MonoBehaviour
{
    Renderer[] characterMaterials;

    public Texture2D[] albedoList;
    [ColorUsage(true, true)]
    public Color[] eyeColors;
    public enum EyePosition { normal, happy, angry, dead }
    public EyePosition eyeState;
    public int colorIndex;

    private void Awake()
    {
        characterMaterials = GetComponentsInChildren<Renderer>();
    }
    

    public void ChangeMaterialSettings(int index)
    {
        for (int i = 0; i < characterMaterials.Length; i++)
        {
            if (characterMaterials[i].transform.CompareTag("PlayerEyes"))
            {
                characterMaterials[i].material.SetColor("_EmissionColor", eyeColors[index]);
            }
            else
            {
                if (!characterMaterials[i].transform.CompareTag("Sword"))
                    characterMaterials[i].material.mainTexture = albedoList[index];
            }
                
        }
        ChangeEyeOffset(EyePosition.happy);
        colorIndex = index;
    }

    void ChangeEyeOffset(EyePosition pos)
    {
        Vector2 offset = Vector2.zero;

        switch (pos)
        {
            case EyePosition.normal:
                offset = new Vector2(0, 0);
                break;
            case EyePosition.happy:
                offset = new Vector2(.33f, 0);
                break;
            case EyePosition.angry:
                offset = new Vector2(.66f, 0);
                break;
            case EyePosition.dead:
                offset = new Vector2(.33f, .66f);
                break;
            default:
                break;
        }

        for (int i = 0; i < characterMaterials.Length; i++)
        {
            if (characterMaterials[i].transform.CompareTag("PlayerEyes"))
                characterMaterials[i].material.mainTextureOffset = offset;
        }
    }

}
