using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRightHandComponent : MonoBehaviour
{
    public GameObject sword;
    private void FindSword()
    {
        GameObject[] swords = GameObject.FindGameObjectsWithTag("Sword");
        Debug.Log($"Tag Sword found: {swords.Length}");
        foreach (GameObject s in swords)
        {
            s.transform.SetParent(transform);
            this.sword = s;
            Vector3 org = transform.position;

            sword.transform.position = org + transform.right * (-0.5f);
            //sword.transform.RotateAround(org, transform.right, 90f);
            sword.transform.rotation = Quaternion.Euler(-90f, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z + 10f);
            break;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        //FindSword();
    }

    // Update is called once per frame
    void Update()
    {
        //if (sword == null)
        //{
        //    FindSword();
        //    return;
        //}
        //else
        //{
            
        //}
    }
}
