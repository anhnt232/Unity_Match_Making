using System.Collections.Generic;
using UnityEngine;
using Mirror;


public class PlayerHealthComponent : NetworkBehaviour
{

    public NetworkAnimator netAnimator;
    public GameObject target;

    [Header("Settings")]
    [SerializeField] private int maxHealth = 100;
    [SerializeField] private int damagePerPress = 10;


    [SyncVar]
    private int currentHealth;

    public delegate void HealthChangeDelegate(int currentHealth, int maxHealth, string attackerName);

    public event HealthChangeDelegate onHealthChanged;

    public bool attack = false;

    [Server]
    private void SetHealth(int value, string attackerName = "None")
    {
        currentHealth = value;
        //Debug.Log("On the Server: Current health: " + currentHealth);
        // harm stuff here

        // invoke on server
        this.onHealthChanged?.Invoke(currentHealth, maxHealth, attackerName);
        // invoke on client
        RpcOnHealthChanged(currentHealth, maxHealth, attackerName);
    }

    public override void OnStartServer()
    {
        base.OnStartServer();
        SetHealth(maxHealth);
    }


    [Command]
    public void CmdAttack(GameObject target)
    {
        //Debug.Log("CmdAttack called on server");
        PlayerHealthComponent h = target.GetComponent<PlayerHealthComponent>();
        PlayerControllerComponent p = this.GetComponent<PlayerControllerComponent>();
        h.SetHealth(Mathf.Max(h.currentHealth - h.damagePerPress, 0), p.GetName());
           
    }


    [ClientRpc]
    private void RpcOnHealthChanged(int currentHealth, int maxHealth, string attackerName)
    {
        this.onHealthChanged?.Invoke(currentHealth, maxHealth, attackerName);
        //Debug.Log("On the Client: Current health: " + currentHealth);

    }

    private void Start()
    {
    }

    [ClientCallback]
    private void Update()
    {
        if (!hasAuthority) { return; }

        if (attack)
        {
            CmdAttack(target);
            attack = false;
        }
    }


}
