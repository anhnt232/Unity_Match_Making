
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using Mirror;

//This script requires you to have setup your animator with 3 parameters, "InputMagnitude", "InputX", "InputZ"
//With a blend tree to control the inputmagnitude and allow blending between animations.
[RequireComponent(typeof(CharacterController))]
public class PlayerMovementInputComponent : NetworkBehaviour
{
    public float Velocity = 12.5f;
    [Space]

	public float InputX;
	public float InputZ;
    public float InputY;
    public Vector3 desiredMoveDirection;
	public bool blockRotationPlayer;
	public float desiredRotationSpeed = 0.1f;
    private Animator animator;
    private NetworkAnimator netAnimator;
    private float Speed;
	public float allowPlayerRotation = 0.1f;
    private Camera mainCamera;
    private MiniMapCameraController miniMapCameraController;
	private CharacterController controller;

    [Header("Animation Smoothing")]
    [Range(0, 1f)]
    public float HorizontalAnimSmoothTime = 0.2f;
    [Range(0, 1f)]
    public float VerticalAnimTime = 0.2f;
    [Range(0,1f)]
    public float StartAnimTime = 0.3f;
    [Range(0, 1f)]
    public float StopAnimTime = 0.15f;

    public float verticalVelocity;
    private Vector3 moveVector;

    private AudioSource[] audioSources = null;
    protected Joystick joystick;

    private JoyButtonO joyButtonO;
    private JoyButtonS joyButtonS;
    private JoyButtonX joyButtonX;
    private JoyButtonT joyButtonT;


    private float buttonOPressTimer;
    private float buttonSPressTimer;
    private float buttonXPressTimer;
    private float buttonTPressTimer;

    private float deadTimer = -1f;

    private bool isGrounded = false;
    private bool isJumping = false;
    private bool isFalling = false;


    private readonly float jumpHeight = 5f;
    private readonly float gravityValue = 9.81f;

    private SwordAttackComponent attackComponent;

    private void Awake()
    {
        joyButtonO = FindObjectOfType<JoyButtonO>();
        joyButtonS = FindObjectOfType<JoyButtonS>();
        joyButtonX = FindObjectOfType<JoyButtonX>();
        joyButtonT = FindObjectOfType<JoyButtonT>();

        mainCamera = Camera.main;
        miniMapCameraController = GameObject.FindGameObjectWithTag("MiniMapCamera").GetComponent<MiniMapCameraController>();
        

    }

    // Use this for initialization
    void Start ()
    {
        Application.targetFrameRate = 30;
        Debug.Log("Start Movement Input " + this.name);
        if (isLocalPlayer)
        {
            miniMapCameraController.targetPlayer = transform;

            animator = this.GetComponent<Animator>();
            animator.applyRootMotion = true;

            netAnimator = this.GetComponent<NetworkAnimator>();
            
            controller = this.GetComponent<CharacterController>();
            joystick = GameObject.FindWithTag("joystick").GetComponent<Joystick>();

            attackComponent = this.GetComponentInChildren<SwordAttackComponent>();

            audioSources = this.GetComponents<AudioSource>();

        }
    }
	
	// Update is called once per frame
	void Update () {

        if (!hasAuthority) return;

        if (deadTimer >= 0)
        {
            deadTimer -= Time.deltaTime;
            if(deadTimer <= 0)
                NetworkServer.Destroy(this.gameObject);
        }

        buttonXPressTimer -= Time.deltaTime;
        buttonOPressTimer -= Time.deltaTime;
        buttonTPressTimer -= Time.deltaTime;
        buttonSPressTimer -= Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            netAnimator.SetTrigger("MeleeAttack");
        }
        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            netAnimator.SetTrigger("ComboMeleeAttack");
        }
        if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            netAnimator.SetTrigger("Dead");
            deadTimer = 5f;
                
        }
            
        if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            netAnimator.SetTrigger("StandingReact");
        }
        if (Input.GetKeyDown(KeyCode.V))
        {
            netAnimator.SetTrigger("BackFlip");
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            netAnimator.SetTrigger("Blocking");
        }
        if (Input.GetKeyDown(KeyCode.Y))
        {
            netAnimator.SetTrigger("JumpHorizontal");
        }
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            //CreateMMOCharacterMessage characterMessage = new()
            //{
            //    username = PlayerPrefs.GetString("username", "kaka"),
            //    teamColor = PlayerPrefs.GetString("color", "")
            //};
            ////Debug.Log("OnClientConnect send Create Char");
            //NetworkClient.Send(characterMessage);

        }
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            netAnimator.SetTrigger("ComboAttack");
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            netAnimator.SetTrigger("JumpOver");
        }

        if (Input.GetKeyDown(KeyCode.Alpha3) || joyButtonX.Pressed)
        {
            if(buttonXPressTimer < 0)
            {
                netAnimator.SetTrigger("JumpHorizontal");
                buttonXPressTimer = 0.2f;
            }
                
        }
        if (Input.GetKeyDown(KeyCode.Alpha4) || joyButtonT.Pressed)
        {
            if (buttonTPressTimer < 0)
            {
                if (attackComponent != null)
                    attackComponent.attackState = true;
                netAnimator.SetTrigger("Slash");
                buttonTPressTimer = 0.2f;
            }
                    
        }
        if (Input.GetKeyDown(KeyCode.Alpha5) || joyButtonS.Pressed)
        {
            if (buttonSPressTimer < 0)
            {
                if (attackComponent != null)
                    attackComponent.attackState = true;
                netAnimator.SetTrigger("MeleeAttackDownward");
                buttonSPressTimer = 0.2f;
            }
                
        }

        if (Input.GetKeyDown(KeyCode.J) || joyButtonO.Pressed)
        {
            if (isGrounded)
            {
                isJumping = true;
                verticalVelocity += Mathf.Sqrt(jumpHeight * 2f * gravityValue);
                netAnimator.SetTrigger("JumpUp");
            }
                
        }

        InputMagnitude();

        if (isGrounded)
        {
            if (isFalling)
            {
                netAnimator.SetTrigger("Landing");
                //Debug.Log($"isGrounded: {isGrounded}, isJumping: {isJumping}, isFalling: {isFalling}, verticalVel: {verticalVelocity}, moveVector: {moveVector}");

                isJumping = false;
                isFalling = false;
            }
            if (verticalVelocity < 0.05)
                verticalVelocity = 0.05f;
        }
        else
        {
            verticalVelocity -= gravityValue * Time.deltaTime;
            if ( isJumping && verticalVelocity < 0 )
            {
                if (!isFalling)
                {
                    netAnimator.SetTrigger("Falling");
                    isFalling = true;
                }
            }
        }
        moveVector = new Vector3(0, verticalVelocity * Time.deltaTime, 0);

        //Debug.Log($"isGrounded: {isGrounded}, isJumping: {isJumping}, isFalling: {isFalling}, verticalVel: {verticalVelocity}, moveVector: {moveVector}, speed: {Speed}, jH: {joystick.Horizontal}, jV: {joystick.Vertical}");

        controller.Move(moveVector);

        isGrounded = controller.isGrounded;
        

    }

	void InputMagnitude() {
        //Calculate Input Vectors
        InputX = Input.GetAxis("Horizontal") > 0 ? 1 : (Input.GetAxis("Horizontal") < 0 ? -1 : 0);
        
        InputZ = Input.GetAxis("Vertical") > 0 ? 1 : (Input.GetAxis("Vertical") < 0 ? -1 : 0);

        if (joystick.Vertical != 0 || joystick.Horizontal != 0)
        {
            InputX = joystick.Horizontal;
            InputZ = joystick.Vertical;
        }

        //Calculate the Input Magnitude
        Speed = new Vector2(InputX, InputZ).sqrMagnitude;

        //Physically move player

        //Debug.Log($"Running blend: {Speed}");

        if (Speed > allowPlayerRotation)
        {
            //netAnimator.SetTrigger("Run");

            animator.SetFloat("RunningBlend", Speed, StartAnimTime, Time.deltaTime);
            PlayerMoveAndRotation ();
            if (audioSources[0] != null)
            {
                if (!audioSources[0].isPlaying)
                {
                    audioSources[0].Play();
                }
            }
        } else if (Speed < allowPlayerRotation)
        {
            animator.SetFloat ("RunningBlend", Speed, StopAnimTime, Time.deltaTime);
            //netAnimator.SetTrigger("Run");

            if (audioSources[0] != null)
            {
                audioSources[0].Stop();
            }
        }
    }

    void PlayerMoveAndRotation()
    {
        var forward = mainCamera.transform.forward;
        var right = mainCamera.transform.right;

        forward.y = 0f;
        right.y =  0f;

        forward.Normalize();
        right.Normalize();

        desiredMoveDirection = forward * InputZ + right * InputX;

        if (blockRotationPlayer == false)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDirection), desiredRotationSpeed);
            controller.Move(desiredMoveDirection * Time.deltaTime * Velocity);
        }
    }
}
