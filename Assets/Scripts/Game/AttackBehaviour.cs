using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.VFX;

public class AttackBehaviour : StateMachineBehaviour
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);

        //Debug.Log($"{animator.transform.name} is called on StateEnter");

        VisualEffect slashEffect = animator.transform.GetComponentInChildren<VisualEffect>();

        AudioSource[] audioSources = animator.transform.GetComponents<AudioSource>();

        //Debug.Log($"slashEffect: {slashEffect}");
        //Debug.Log($"audioSources: {audioSources}");

        if (slashEffect!= null)
        {
            slashEffect.Play();
        }
        if(audioSources != null && audioSources.Length > 1)
        {
            if (!audioSources[1].isPlaying)
            {
                audioSources[1].Play();
            }
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);

        //Debug.Log($"{animator.transform.name} is called on OnStateExit");

        VisualEffect slashEffect = animator.transform.GetComponentInChildren<VisualEffect>();

        AudioSource[] audioSources = animator.transform.GetComponents<AudioSource>();

        //Debug.Log($"slashEffect: {slashEffect}");
        //Debug.Log($"audioSources: {audioSources}");

        if (slashEffect != null)
        {
            slashEffect.Stop();
        }
        if (audioSources != null && audioSources.Length > 1)
        {
            audioSources[1].Stop();
        }
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
