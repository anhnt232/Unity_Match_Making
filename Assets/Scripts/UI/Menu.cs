﻿
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[System.Serializable]
public class RoomEvent : UnityEvent<string> { };
public class Menu : MonoBehaviour
{

    public RoomEvent onPlayGame = new RoomEvent();
    public RoomEvent onChangeUsername = new RoomEvent();
    public RoomEvent onChangeColor = new RoomEvent();
    public RoomEvent onChangeNext = new RoomEvent();
    public RoomEvent onChangePres = new RoomEvent();

    public TMP_Text gameTitle;
    public TMP_InputField userNameInput;
    public Button redButton;
    public Button blackButton;
    public Button blueButton;
    public Button yellowButton;
    public Button playButton;
    public Button nextButton;
    public Button presButton;

    private CharacterImageControl characterImageControl;

    private readonly string[] usernames = { "Coco",
        "Maya", "Koda", "Dexter", "Benji" };

    public void Init()
    {
        string username = PlayerPrefs.GetString("username");
        string teamColor = PlayerPrefs.GetString("teamColor", "red");
        string heroName = PlayerPrefs.GetString("heroName");

        if (username.Length > 0)
        {
            userNameInput.text = username;
        }
        else
        {
            userNameInput.text = usernames[Random.Range(0, usernames.Length)];
        }

        if (heroName.Length > 0)
        {
            characterImageControl = GameObject.FindWithTag("CharacterSelectionImage").GetComponent<Image>().GetComponent<CharacterImageControl>();

            int s = characterImageControl.DisplayCharacterImage(heroName);
            PlayerPrefs.SetInt("currentHeroIdx", s);
        }


        redButton.onClick.AddListener(() => onChangeColor.Invoke("red"));

        blackButton.onClick.AddListener(() => onChangeColor.Invoke("black"));

        blueButton.onClick.AddListener(() => onChangeColor.Invoke("blue"));

        yellowButton.onClick.AddListener(() => onChangeColor.Invoke("yellow"));

        if (teamColor == "red")
        {
            redButton.Select();
            redButton.onClick.Invoke();
        }
        if (teamColor == "black")
        {
            blackButton.Select();
            blackButton.onClick.Invoke();
        }
        if (teamColor == "blue")
        {
            blueButton.Select();
            blueButton.onClick.Invoke();
        }
        if (teamColor == "yellow")
        {
            yellowButton.Select();
            yellowButton.onClick.Invoke();
        }

        userNameInput.onEndEdit.AddListener((string text) => { onChangeUsername.Invoke(userNameInput.text); });

        userNameInput.onEndEdit.Invoke(userNameInput.text);

        nextButton.onClick.AddListener(() => onChangeNext.Invoke(""));

        presButton.onClick.AddListener(() => onChangePres.Invoke(""));

        playButton.onClick.AddListener(() => onPlayGame.Invoke(userNameInput.text));
    }

    
}
