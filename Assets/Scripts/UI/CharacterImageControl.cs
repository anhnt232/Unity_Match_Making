using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class CharacterImageControl : MonoBehaviour
{
    public Sprite[] sprites;

    public string[] heroNames = { "Jammo", "Asuna", "Rfa",
        "Kachujin", "Knight", "KnightKit", "Ninja", "Dreyar", "Ely" };

    public TMP_Text textHeroName;

    public Image characterImage;

    // Start is called before the first frame update
    void Awake()
    {
        characterImage = GetComponent<Image>();
    }

    public int DisplayCharacterImage(int idx)
    {
        int _idx = idx;

        if (_idx >= heroNames.Length)
        {
            _idx = 0;
        }
        if (_idx < 0)
        {
            _idx = heroNames.Length - 1;
        }
        if(heroNames[_idx] == "Jammo" || heroNames[_idx] == "KnightKit")
        {
            characterImage.rectTransform.sizeDelta = new Vector2(250, 300);
        }
        else
        {
            characterImage.rectTransform.sizeDelta = new Vector2(200, 300);
        }
        characterImage.sprite = sprites[_idx];
        textHeroName.text = heroNames[_idx];

        PlayerPrefs.SetString("heroName", heroNames[_idx]);

        return _idx;
    }

    public int DisplayCharacterImage(string heroName)
    {
        for(int i = 0; i < heroNames.Length; i++)
        {
            if (heroNames[i] == heroName)
            {
                DisplayCharacterImage(i);
                return i;
            }
        }
        return 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
