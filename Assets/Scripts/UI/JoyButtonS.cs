using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class JoyButtonS : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    public bool Pressed;
    private float buttonTimer;
    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        if (!Pressed)
        {
            Pressed = true;
            buttonTimer = 0.2f;
        }

    }

    void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
    {
        if (buttonTimer < 0)
            Pressed = false;
    }

    void Update()
    {
        if (buttonTimer < 0)
        {
            buttonTimer = 0;
            Pressed = false;
        }
        buttonTimer -= Time.deltaTime;
    }
    public bool isPressed()
    {
        if(buttonTimer > 0)
        {
            return false;
        }
        else
        {
            return Pressed;
        }
    }
}
