using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestProjectile : MonoBehaviour
{
    public static TestProjectile instance;
    public float speed;
    public float timeOff = 1f;
    private void OnEnable() {
        Invoke("DestroyProjectile", timeOff);
    }
    private void Awake() {
        if(instance == null) {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        // Invoke("DestroyProjectile", 1f);
    }

    // Update is called once per frame
    void Update()
    {
        // transform.Translate(transform.forward * speed * Time.deltaTime);


    }
    void FixedUpdate() {
        GetComponent<Rigidbody>().velocity = transform.forward * speed;
    }

    void DestroyProjectile() {
        // gameObject.SetActive(false);
        // ObjectPoolPro.instance.ReturnToPool("HammerProjectile", gameObject);
        // ObjectPool.instance.ReturnToPool(gameObject);
        Destroy(gameObject);
    }
}
