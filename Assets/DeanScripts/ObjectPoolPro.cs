using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolPro : MonoBehaviour
{
    public static ObjectPoolPro instance;

    private void Awake() {
        if(instance == null) {
            instance = this;
        }
    }
    [System.Serializable]
    public class Pool {
        public int poolCount;
        public GameObject poolObject;
        public string tag;
        public bool canGrow;
    }

    public List<Pool> pools = new();
    Dictionary<string, Queue<GameObject>> objectPools = new ();
    // Start is called before the first frame update
    void Start()
    {
        foreach(Pool pool in pools) {
            Queue<GameObject> q = new();
            for (var i = 0; i < pool.poolCount; i++)
            {
                GameObject obj = Instantiate(pool.poolObject);
                obj.SetActive(false);
                q.Enqueue(obj);
            }
            objectPools.Add(pool.tag, q);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public GameObject GetFromPool(string tag)
    {
        var poolGrow = false;
        GameObject poolObject = null;
        pools.ForEach(item =>
        {
            if(item.tag == tag) {
                poolGrow = item.canGrow;
                poolObject = item.poolObject;
            }
        });
        if (objectPools[tag].Count > 0)
            {
                return objectPools[tag].Dequeue();
            }
            else if(poolGrow) {
            GameObject obj = Instantiate(poolObject);
            return obj;
        } 
        else
        {
            return null;
        }
    }

    public void ReturnToPool(string tag, GameObject obj)
    {
        objectPools[tag].Enqueue(obj);
    }
}
