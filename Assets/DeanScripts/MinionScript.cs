using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionScript : MonoBehaviour
{
    public float speed;
    public float timeOff = 2f;

    private Animator anim;

    private void OnEnable()
    {
        Invoke("DestroyMinion", timeOff);
    }
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        // Invoke("DestroyMinion", 1f);
    }

    // Update is called once per frame
    void Update()
    {
        // transform.Translate(transform.forward * speed * Time.deltaTime);

        GetComponent<Rigidbody>().velocity = transform.forward * speed;
        anim.SetBool(name: "Walk Forward", true);

    }

    void DestroyMinion()
    {
        gameObject.SetActive(false);
        anim.SetBool(name: "Walk Forward", false);
        ObjectPoolPro.instance.ReturnToPool("RedSpider", gameObject);
        // ObjectPool.instance.ReturnToPool(gameObject);
        // Destroy(gameObject);
    }
}
