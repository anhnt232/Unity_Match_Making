using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VFX;

public enum NewAbilityType
{
    AOE,
    PROJECTILE
}

public enum NewMarkerType
{
    LOCKED,
    FREE
}
[System.Serializable]
public class NewVFXAsset
{
    public string VFXName;
    public GameObject VFXPrefab;
    [Tooltip("If a fire point is not provided it will spawn in the VFXMarker position.")]
    public Transform VFXFirePoint;
    [Tooltip("Used in case we want to parent the VFXPrefab.")]
    public Transform VFXParent;
    [Tooltip("Offsets the VFXPrefab a certain amount.")]
    public Vector3 VFXOffset;
    [Space]
    [Tooltip("Use for AoE skill, it will only rotate towards where we are aiming otherwise we aim freely.")]
    public bool rotateToDestination = false;
    [Tooltip("How many seconds to initiate VFX Prefab?")]
    public float delayToStart = 10;
    [Tooltip("Destroy the instantiated vfx after how many seconds?")]
    public float delayToDestroy = 10;
    public List<AudioClip> SFX;
}
[System.Serializable]
public class NewVFXParameters
{
    public string AbilityName;
    [Tooltip("Joystick conttroller for ability skill")]
    public zFrame.UI.Joystick joystickSkill;
    [Tooltip("Ability types. AOE doesnt need firepoint or projectile speed")]
    public AbilityType abilityType;
    [Tooltip("Cooldown before shooting this ability again")]
    public float cooldown = 2;
    [Tooltip("How much time is the character immovable to cast the spell?")]
    public float freezeTime;
    [Tooltip("The travelling speed of the projecitle. Only for Projectiles.")]
    public float projectileSpeed;
    [Tooltip("Attack range for skill")]
    public float attackRange;
    [Space]
    [Tooltip("Should the ground marker be Free or Locked? Where locked is at the foot of the character and free is within a certain radius.")]
    public MarkerType markerType;

    [Tooltip("The Ground Marker Prefab")]
    public GameObject VFXMarker;
    [Tooltip("The Circle Range Attack Prefab")]
    public GameObject VFXAttackCircleRange;
    [Tooltip("The Ground Marker position. Only for Projectiles. Also used for generate Circle Range Attack")]
    public Transform VFXMarkerPosition;
    [Space]
    [Tooltip("The VFX Asset is to decalre the prefab, firepoint, parent and offset")]
    public List<NewVFXAsset> VFXAsset;
}

public class SpawnSkill : MonoBehaviour
{
    public NewVFXParameters firstSkill;
    private bool firstSkillFire;
    private Animator anim;
    private GameObject[] skillSpawnPrefabs;
    private GameObject projectileVFXPrefab;

    void Awake() {
        firstSkill.VFXMarker = Instantiate(firstSkill.VFXMarker) as GameObject;
        firstSkill.VFXAttackCircleRange = Instantiate(firstSkill.VFXAttackCircleRange) as GameObject;
        anim = GetComponent<Animator>();
        GameObject.FindGameObjectWithTag("AttackButton").GetComponent<Button>().onClick.AddListener(NewShoot);

        firstSkill.joystickSkill.OnValueChanged.AddListener(v =>
        {
            if (v.magnitude != 0)
            {
                firstSkill.VFXMarker.SetActive(true);
                var inputX = v.x;
                var inputZ = v.y;
                firstSkill.VFXMarker.transform.position = firstSkill.VFXMarkerPosition.transform.position;
                float angle = Mathf.Atan2(inputZ, inputX) * Mathf.Rad2Deg - 90;
                firstSkill.VFXMarker.transform.rotation = Quaternion.Euler(new Vector3(0, -angle, 0));
            }
        });
        firstSkill.joystickSkill.OnPointerUp.AddListener(v =>
        {
            firstSkill.VFXMarker.SetActive(false);
            transform.rotation = Quaternion.Slerp(transform.rotation, firstSkill.VFXMarker.transform.rotation, 4f);

            // Circle Range Attack
            firstSkill.VFXAttackCircleRange.SetActive(false);
            NewShoot();
        });
        firstSkill.joystickSkill.OnPointerDown.AddListener(v =>
        {
            if (v.magnitude != 0)
            {
                firstSkill.VFXMarker.SetActive(true);
                var inputX = v.x;
                var inputZ = v.y;
                firstSkill.VFXMarker.transform.position = firstSkill.VFXMarkerPosition.transform.position;
                firstSkill.VFXMarker.transform.rotation = transform.rotation;


                // Circle Range Attack
                firstSkill.VFXAttackCircleRange.SetActive(true);
                Vector3 circlePos = firstSkill.VFXMarkerPosition.transform.position;
                circlePos.y = 0.1f;
                firstSkill.VFXAttackCircleRange.transform.position = circlePos;
                generateCircleMarker(firstSkill.attackRange, firstSkill.VFXAttackCircleRange, firstSkill.VFXMarker);
            }

        });
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        TrackAndDestroyPrefab();
    }
    private void generateCircleMarker(float circleRange, GameObject circleMarkerPrefab, GameObject skillMarkerPrefab) {
        // Circle Marker
        var circleContainerPrefab = circleMarkerPrefab.transform;
        var circleMarker = circleContainerPrefab.Find("vfxgraph_HUDCircleMarker");
        var sphereChild = circleContainerPrefab.Find("CircleRangeSphere");

        var vfxCirlce = circleMarker.GetComponent<VisualEffect>();
        vfxCirlce.SetFloat("CircleRadius", circleRange);
        sphereChild.transform.localScale = new Vector3(circleRange, circleRange, circleRange);


        // // Skill Marker
        var skillContainerPrefab = skillMarkerPrefab.transform;
        var skillMarker = skillContainerPrefab.Find("vfxgraph_HUDMarker");
        var vfxSkillMarker = skillMarker.GetComponent<VisualEffect>();
        Vector3 vfxSkillSize = vfxSkillMarker.GetVector3("MarkerSize");
        vfxSkillSize.y = circleRange / 2;
        vfxSkillMarker.SetVector3("MarkerSize", vfxSkillSize);
        // Debug.Log("Vfx Skill Pos" + vfxSkillSize);

    }
    private void NewShoot()
    {
        if (anim != null && anim.ContainsParam("Attack01"))
            anim.SetTrigger("Attack01");
    }
    private void TrackAndDestroyPrefab()
    {
        if(projectileVFXPrefab != null)
        {
            if (Vector3.Distance(projectileVFXPrefab.transform.position, firstSkill.VFXMarkerPosition.transform.position) > firstSkill.attackRange / 2 - 0.2)
            {
                Debug.Log(Vector3.Distance(projectileVFXPrefab.transform.position, firstSkill.VFXMarkerPosition.transform.position));
                Debug.Log(firstSkill.attackRange);
                Destroy(projectileVFXPrefab);
            }
        }
    }

    public void GenerateProjectile() {
        firstSkill.VFXAsset.ForEach(item =>
        {
            if (item.VFXName == "Hammer Projectile")
            {
                item.VFXPrefab.SetActive(true);
                GameObject vfxProjectile = Instantiate(item.VFXPrefab, item.VFXFirePoint.transform.position, gameObject.transform.rotation) as GameObject;
                vfxProjectile.transform.rotation = gameObject.transform.rotation;
                firstSkillFire = false;
                vfxProjectile.GetComponent<Rigidbody>().velocity = transform.forward * firstSkill.projectileSpeed;
                projectileVFXPrefab = vfxProjectile;
                anim.ResetTrigger("Attack01");

                StartCoroutine(DestroyProjectile(vfxProjectile, item.delayToDestroy));

            } else {
                if (item.VFXName == "Warm Up")
                {
                    item.VFXPrefab.SetActive(false);
                    //DestroyImmediate(item.VFXPrefab, true);
                }
 
            }
        });
    }
    IEnumerator DestroyProjectile(GameObject obj, float time){
        yield return new WaitForSeconds(time);
        Destroy(obj);
    }
    public void GenerateWeaponGlow() {
        firstSkill.VFXAsset.ForEach(item =>
        {
            if (item.VFXName != "Hammer Projectile")
            {
                GameObject vfxProjectile = Instantiate(item.VFXPrefab, item.VFXFirePoint.transform.position + item.VFXOffset, Quaternion.identity) as GameObject;
                vfxProjectile.SetActive(true);
                if (item.VFXParent != null)
                {
                    vfxProjectile.transform.SetParent(item.VFXParent);
                    vfxProjectile.transform.localPosition = Vector3.zero;
                    vfxProjectile.transform.localEulerAngles = Vector3.zero;
                }
            }
        });
    }

    public void DeleteAllVFXPrefab() {
        skillSpawnPrefabs = GameObject.FindGameObjectsWithTag("ThunderLord_Skill");
        foreach (var item in skillSpawnPrefabs)
        {
            if(item.name != "vfx_Ab_TestProjectileHammer(Clone)") {
                Destroy(item);  
            }
        }
    }

    // IEnumerator UseSkill() {
    //     yield return new WaitForSeconds(2f);
    //     GameObject vfxProjectile = Instantiate(skillProjectile, skillSpawnPoint.position, gameObject.transform.rotation) as GameObject;
    //     vfxProjectile.transform.rotation = markerSkill1.transform.rotation;
    //     gameObject.transform.rotation = markerSkill1.transform.rotation;
    //     yield return new WaitForSeconds(2f);
    //     skillProjectileFire1 = false;
    //     Destroy(vfxProjectile);
    // }

    // private void Shoot()
    // {
    //    GameObject vfxProjectile = Instantiate(skillProjectile, skillSpawnPoint.position, gameObject.transform.rotation) as GameObject;
    //     vfxProjectile.transform.rotation = gameObject.transform.rotation;
    //     // skillProjectileFire1 = false;
    // }
    // private void ShootFromPool() {
    //     GameObject projectile = ObjectPool.instance.GetFromPool();
    //     projectile.SetActive(true);
    //     projectile.transform.position = skillSpawnPoint.position;
    //     projectile.transform.rotation = gameObject.transform.rotation;
    // }
    // private void ShootFromPoolPro() {
    //     GameObject projectile = ObjectPoolPro.instance.GetFromPool("HammerProjectile");
    //     projectile.SetActive(true);
    //     projectile.transform.position = skillSpawnPoint.position;
    //     projectile.transform.rotation = gameObject.transform.rotation;
    // }

    // private void ShowMarkerSkill(){
    //     if(joystickSkill1 != null) {
    //         // Debug.Log(joystickSkill1.Direction);
    //         if(joystickSkill1.Horizontal != 0f || joystickSkill1.Vertical != 0f) {
    //             markerSkill1.SetActive(true);
    //             var inputX = joystickSkill1.Horizontal;
    //             var inputZ = joystickSkill1.Vertical;
    //             markerSkill1.transform.position = markerPosition.transform.position;
    //             float angle = Mathf.Atan2(inputZ, inputX) * Mathf.Rad2Deg - 90;
    //             // Debug.Log("Angle" + angle);
    //             markerSkill1.transform.rotation = Quaternion.Euler(new Vector3(0, -angle, 0));
    //             Debug.Log("Joystick" + joystickSkill1.FingerIsDown);
    //             // if(joystickSkill1.FingerIsDown) {
    //             //     Debug.Log("Finder down");
    //             //     skillProjectileFire1 = true;
    //             // } else {
    //             //     Debug.Log("Finder up");
    //                 skillProjectileFire1 = true;

    //             // }
    //             // skillProjectile.SetActive(true);
    //         } else {
    //             // skillProjectile.SetActive(false);
    //             // skillProjectileFire1 = true;
    //             markerSkill1.SetActive(false);
    //         }
    //     }
    // }

}
