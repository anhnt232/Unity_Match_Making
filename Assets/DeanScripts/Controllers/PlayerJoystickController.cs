using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJoystickController : MonoBehaviour
{
    public FixedJoystick moveJoystick;

    // Update is called once per frame
    void Update()
    {
        float horizontal = moveJoystick.Horizontal;
        float vertical = moveJoystick.Vertical;
        Vector3 direction = new Vector3(horizontal, 0, vertical).normalized;
        transform.Translate(direction * 0.02f, Space.World);
    }
}
