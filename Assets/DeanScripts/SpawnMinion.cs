using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnMinion : MonoBehaviour
{
    public GameObject minionObject;
    public Transform minionSpawnPoint;
    public float respawnTime = 3.0f;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(minionWave());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void spawnMinionFromPoolPro()
    {
        //GameObject minion = ObjectPoolPro.instance.GetFromPool("RedSpider");
        //minion.SetActive(true);
        //minion.transform.position = minionSpawnPoint.position;
        //minion.transform.rotation = minionSpawnPoint.rotation;
    }

    IEnumerator minionWave() {
        while(true) {
            yield return new WaitForSeconds(respawnTime);
            spawnMinionFromPoolPro();
        }
    }
}
